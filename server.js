require('dotenv').config();
const path = require('path');
const mongoose = require('mongoose');

const express = require('express');
const listEndpoints = require('express-list-endpoints')
const exphbs  = require('express-handlebars');
const logger = require('morgan');


mongoose.connect('mongodb+srv://user:5150kuro@cluster0.uedxq.mongodb.net/nodekb', {
});

const db = mongoose.connection;

db.once('open', function(){
    console.log("Connected to MongoDB")
});

db.once('error', function(error){
    console.log("error : " + error)
});

const app = express();

let { Article } = require('./models/articles');
// const Article = require('./models/articles')

app.use(logger('dev'));

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// app.engine('handlebars', exphbs());
// app.set('view engine', 'handlebars');
// app.set('views', path.join(__dirname, 'views/'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
    Article.find({}, function(err, articles){
        if(err){
            console.log()
        }else{
            res.render('index', {
                title:'index',
                articles:articles
            });
        }
    });
});

app.get('/article/:id', async function(req, res) {
    const article = await Article.findById(req.params.id);

    res.render('article', {
        title: 'Article',
        article: article
      });
});

app.get('/articles/add', function(req, res) {
    res.render('add_article', {
      title:'Add Article'
    });
  });

app.post('/articles/add', function(req, res) {
    const article = new Article();
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;

    article.save(function(error){
        if(error){
            console.log("error : " + error)
        }else{
            res.redirect('/')
        }
    });

    req.flash('success', 'Article Added');
    res.redirect('/');
});

app.get('/article/edit/:id', async (req, res) => {
    const article = await Article.findById(req.params.id);

    res.render('edit_article', {
        title: 'Edit Article',
        article: article
      });
  });

app.post('/article/edit/:id', async (req, res) => {
    try {
        const article = {
            title: req.body.title,
            author: req.body.name,
            body: req.body.body
        };

        let query = { _id: req.params.id }
        const update = await Article.update(query, article);
        res.redirect('/');
        // if (update) {
        // req.flash('success', 'Article Updated');
        // res.redirect('/');
        // } return;

    } catch (e) {
        res.send(e);
    }
});

app.delete('/article/:id', async (req, res) => {
    try {
        let query = { _id: req.params.id }
        const article = await Article.findById(req.params.id);
        remove = await Article.findByIdAndRemove(query);

        res.send('Success');
    } catch (e) {
        res.send(e);
    }
});

app.listen(process.env.BACKEND_PORT, function() {
  console.log('Example app listening on port 3000!');
  console.log("process.env : " + JSON.stringify(process.env))
  console.log(listEndpoints(app));
  console.log('http://192.168.33.10:3000');
});